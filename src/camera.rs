use amethyst::{
    core::{math::RealField, Float, Transform},
    ecs::{Join, Read, ReadStorage, System, WriteStorage},
    input::{InputHandler, StringBindings},
    prelude::*,
    renderer::Camera,
};

/// Allows to rotate the camera with the arrow keys.
pub struct CameraSystem;

impl CameraSystem {
    pub const NAME: &'static str = "camera_system";
}

impl<'s> System<'s> for CameraSystem {
    type SystemData = (
        WriteStorage<'s, Transform>,
        ReadStorage<'s, Camera>,
        Read<'s, InputHandler<StringBindings>>,
    );

    fn run(&mut self, (mut transforms, cameras, input): Self::SystemData) {
        for (_camera, transform) in (&cameras, &mut transforms).join() {
            if let Some(mv) = input.axis_value("updown") {
                if mv.is_normal() {
                    transform.append_rotation_x_axis(mv / 10.);
                }
            }
            if let Some(mv) = input.axis_value("leftright") {
                if mv.is_normal() {
                    transform.append_rotation_y_axis(-mv / 10.);
                }
            }
        }
    }
}

/// Setups the camera.
pub fn initialize(world: &mut World) {
    let transform = {
        let mut t = Transform::default();
        t.set_translation_xyz(0., 0., 1.)
            .set_rotation_y_axis(-std::f32::consts::FRAC_PI_2 + 1.2)
            .set_rotation_x_axis(0.1);
        t
    };

    world
        .create_entity()
        .with(Camera::standard_3d(10., 10.))
        .with(transform)
        .build();
}
