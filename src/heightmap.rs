use amethyst::{
    assets::{Handle, Loader},
    core::transform::Transform,
    prelude::*,
    renderer::{
        rendy::mesh::{MeshBuilder, Normal, Position, TexCoord},
        types::MeshData,
        ImageFormat, Material, MaterialDefaults, Mesh,
    },
};

pub fn initialize(world: &mut World) {
    initialize_light(world);

    let vertices = vec![
        Position([0., 0., 0.]),
        Position([1., 0., 0.]),
        Position([0., 1., 0.]),
    ];
    let norm = vec![
        Normal([0., 0., 1.]),
        Normal([0., 0., 1.]),
        Normal([0., 0., 1.]),
    ];
    let tex = vec![TexCoord([0., 0.]), TexCoord([1., 1.]), TexCoord([0., 1.])];

    let (mesh, material): (Handle<Mesh>, Handle<Material>) = {
        let loader = world.read_resource::<Loader>();

        let mesh_store = &world.read_resource();
        let material_store = &world.read_resource();
        let texture_store = &world.read_resource();

        let mesh = MeshBuilder::new()
            .with_vertices(vertices)
            .with_vertices(norm)
            .with_vertices(tex);

        let texture = loader.load("tex.png", ImageFormat::default(), (), texture_store);
        let material = Material {
            albedo: texture,
            ..world.read_resource::<MaterialDefaults>().0.clone()
        };

        (
            loader.load_from_data(MeshData::from(mesh), (), mesh_store),
            loader.load_from_data(material, (), material_store),
        )
    };

    world
        .create_entity()
        .with(mesh)
        .with(material)
        .with(Transform::default())
        .build();
}

pub fn initialize_light(world: &mut World) {
    use amethyst::renderer::{
        light::{Light, PointLight},
        palette::Srgb,
    };

    let light = PointLight {
        color: Srgb::new(1., 1., 1.),
        intensity: 0.5,
        radius: 10.,
        smoothness: 1.,
    };
    let transform = {
        let mut t = Transform::default();
        t.set_translation_xyz(0., 0., 1.);
        t
    };

    world
        .create_entity()
        .with(Light::from(light))
        .with(transform)
        .build();
}
