mod camera;
mod heightmap;
mod rendering;

use amethyst::{
    core::transform::TransformBundle, prelude::*, renderer, utils::application_root_dir,
    assets::{Processor},
    renderer::SpriteSheet,
    window::WindowBundle,
};
use camera::CameraSystem;
use rendering::RenderingGraph;

type InputBundle = amethyst::input::InputBundle<amethyst::input::StringBindings>;
type RenderingSystem<T> = renderer::RenderingSystem<renderer::types::DefaultBackend, T>;

struct MyState;

impl SimpleState for MyState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        let world = data.world;

        camera::initialize(world);
        heightmap::initialize(world);
    }
}

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(Default::default());

    let app_root = application_root_dir()?;

    let resources_dir = app_root.join("resources");
    let display_config = resources_dir.join("display_config.ron");
    let bindings_config = resources_dir.join("bindings_config.ron");

    let game_data = GameDataBuilder::default()
        .with_bundle(WindowBundle::from_config_path(display_config))?
        .with_bundle(TransformBundle::new())?
        .with_bundle(InputBundle::new().with_bindings_from_file(bindings_config)?)?
        .with(CameraSystem, CameraSystem::NAME, &["input_system"])
        .with(
            Processor::<SpriteSheet>::new(),
            "sprite_sheet_processor",
            &[],
        )
        .with_thread_local(RenderingSystem::new(RenderingGraph::default()));

    Application::new(resources_dir, MyState, game_data)?.run();

    Ok(())
}
